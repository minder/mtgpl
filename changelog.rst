
***********
Spis zmian
***********

2020-03-02
   * Zaktualizowne tłumaczenia: ELD, THB

2020-02-27
   * Zaktualizowane tłumaczenia: WAR, M20

2020-02-25
   * Zaktualizowane tłumaczenia: GRN, RNA

2020-02-24
   * Kaladesh - 100%
   * Zmiana znaczników postępu tłumaczeń

2020-02-23
   * Aether Revolt - 100%
   * Kaladesh - 50%
   * Zaktualizowane tłumaczenia: RIX, DOM

2020-02-21
   * Zaktualizowane tłumaczenia: XLN

2020-02-20
   * Amonkhet - 100%
   * Hour of Devastation - 100% + aktualizacja

2020-02-18
   * Eldritch Moon - 100%

2020-02-17
   * Shadows over innistrad - 100%
   * Oatch of the Gatewatch - 100%

2020-02-15
   * Battle for Zendikar - 100%

2020-02-04
   * Origins - 100%

2020-01-30
   * Nowy dodatek: Commander 2019 - 50%
   * Wyzwanie "Pokonaj Bóstwo" - 100%
   * Zaktualizowane tłumaczenia do dwóch wyzwań z bloku Therosa (Hydra i Horda)

2020-01-28
   * Core Set 2015 - 100%
   * Zaktualizowane tłumaczenia: M19

2020-01-27
   * Dragons of Tarkir - 100%
   * Khans of Tarkir - 100%
   * Zaktualizowane tłumaczenia: FRF, M14

2020-01-25
   * Journey into Nyx - 100%
   * Zaktualizowane tłumaczenia: RTR, GTC, DGM, THS, BNG

2020-01-20
   * Throne of Eldraine - 100%


2020-01-19
    * Theros: Beyond Death - 100%
    * Throne of Eldraine - 60%

2020-01-12
    * Throne of Eldraine - 50%
    * Theros: Beyond Death - 25%

2019-04-30
    * War of the Spark - 100%

2019-03-31
    * Guild Kits 2 - 0%
    * Modern Horizons - 0%

2019-02-10
    * Explorers of Ixalan - 100%
    * Amonkhet - 50%

2019-01-13
    * Wystawienie seriwsu dla tłumaczy: https://weblate.mtgpopolsku.pl
    * Aktywacja SSL w całym serwisie, wyłączenie możliwości przeglądania serwisu bez HTTPS.
    * Ustalenie ostatecznej wersji tlumaczenia słów 'tap/untap'.
    * Dodanie symboli zestawów przy linkach i w nagłówkach stron.
    * Przywrócenie możliwości podglądu kart przy najechaniu myszą na nazwę.
    * Dodanie do :doc:`glosariusza<glossary>` linków do zasad w serwisie yawgatog.com
    * Drobna reorganizacja strony startowej.
    * Dodanie opisu formatu :doc:`Handicap<handicap>`.
    * Pełne tłumaczenie :doc:`Ravnica Allegiance<RNA>`.
    * Zmiana źródła danych na Scryfall, lepsze obrazki w tooltipach.

2019-01-06
    * Dodane tłumaczenia przyszłych kart z Ravnica Allegiance (dzięki API Scryfall.com)

2019-01-04
    * Nowy :doc:`podręcznik gry<rulebook>` (edycja Dominaria). Drobne porządki.

2018-12-30
    * :doc:`Dominaria<DOM>` 100%. :doc:`Core Set 2019<M19>` 100%.

2018-12-29
    * :doc:`Ixalan<XLN>` 100%, :doc:`Dominaria<DOM>` 66%.

2018-12-28
    * Testowe uruchomienie strony mtgpopolsku.pl - w pełni przetłumaczone dodatki z aktualnego Standardu: :doc:`Guilds of Ravnica<GRN>`, :doc:`Rivals of Ixalan<RIX>` oraz częściowo pozostałe dodatki aż do :doc:`New Phyrexia<NPH>`.
    * Zmiana tłumaczenia słowa "tap" - mimo że "zaznaczyć" (i analogicznie "odznaczyć" dla "untap") jest bardziej po polsku, to akurat to słowo jest mocno zakorzenione w slangu i występuje niezmienione w innych lokalizacjach gry.

2014-01-29
    * Nowe dodatki: :doc:`Born of the Gods<BNG>`, :doc:`Avacyn Restored<AVR>`, :doc:`Dark Ascension<DKA>`, :doc:`Innistrad<ISD>`, :doc:`New Phyrexia<NPH>`.

2014-01-27
    * Zmiana kolejności wyświetlania dodatków. Dodanie podziału na Standard/Modern/Pozostałe/Specjalne.

2014-01-24
    * Dodano talię wyzwania :doc:`Walka z Hordą<BNG_horde>`.

2013-11-26
    * Dodano surową wersję pełnej instrukcji do gry (Comprehensive Rules) (bez tłumaczenia)
    * Zmiana tłumaczenia słowa "permanent".

2013-11-03
    * Dodano :doc:`zasady formatu Commander<commander>` (bez tłumaczenia).
    * Reorganizacja strony tytułowej.
    * Dodana możliwość włączania podglądu kart w formie tooltip.

2013-10-07
    * Zakończenie tłumaczenia: :doc:`RTR<RTR>`, :doc:`GTC<GTC>`, :doc:`DGM<DGM>`.

2013-09-29
    * Dodano artykuł opisujący dodatkowe warianty gry nie ujęte w Skróconej Instrukcji.
    * Dodano polskie żetony stworów oraz lądy.

2013-09-28
    * Dodanie kart z decku :doc:`Face the Hydra<THS_hydra>` wraz z instrukcją gry.
    * Dodano :doc:`uzasadnienie<rationale>` tłumaczeń określonych słów kluczowych i ogólnej koncepcji projektu.

2013-09-17
    * Zakończenie tłumaczenia :doc:`THS<THS>`.

2013-09-08
    * Adaptacja skróconej instrukcji do formatu używanego podczas tłumaczenia.

2013-09-03
    * Zakończenie tłumaczenia :doc:`M14<M14>`.
    
2013-09-01
    * Start serwisu.

2012-12-01
    * Rozpoczęcie procesu tłumaczenia na `forum Strefy Gry <http://strefa-gry.pl/index.php?/topic/6-tlumaczenie-mtg-czesc-1-typy-cechy-i-zdolnosci-kart/>`_

