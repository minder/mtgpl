*******
Pionier
*******

.. include:: progress.rst

Standard
--------

Format Standard jest rotującym formatem, tzn. raz w roku najstarsze dodatki
obecne w tym formacie przestają być "legalne" - zostają wyłączone z formatu,
czyli "rotują". Na ich miejsce przychodzą nowo wydawane dodatki.

Lista dodatków obecnych w aktualnym Standardzie znajduje się
:doc:`na stronie głównej<index>`.

Karty z dodatków, które wyszły ze Standardu, nadal mogą być grane w formatach
:doc:`Pionier<pioneer>`, :doc:`Modern<modern>` i :doc:`Legacy<legacy>`, a format
Standard jest ich podzbiorem.

Dodatki poza Standardem
-----------------------

* :mtgexp:`DOM` :doc:`Dominaria<DOM>` |p100|
* :mtgexp:`RIX` :doc:`Rivals of Ixalan<RIX>` |p100|
* :mtgexp:`XLN` :doc:`Ixalan<XLN>` |p100|
* :mtgexp:`HOU` :doc:`Hour of Devastation<HOU>` |p100|
* :mtgexp:`AKH` :doc:`Amonkhet<AKH>` |p100|
* :mtgexp:`AER` :doc:`Aether Revolt<AER>` |p100|
* :mtgexp:`KLD` :doc:`Kaladesh<KLD>` |p100|
* :mtgexp:`EMN` :doc:`Eldritch Moon<EMN>` |p100|
* :mtgexp:`SOI` :doc:`Shadows over Innistrad<SOI>` |p100|
* :mtgexp:`OGW` :doc:`Oath of the Gatewatch<OGW>` |p100|
* :mtgexp:`BFZ` :doc:`Battle for Zendikar<BFZ>` |p100|
* :mtgexp:`ORI` :doc:`Origins<ORI>` |p100|
* :mtgexp:`DTK` :doc:`Dragons of Tarkir<DTK>` |p100|
* :mtgexp:`FRF` :doc:`Fate Reforged<FRF>` |p100|
* :mtgexp:`KTK` :doc:`Khans of Tarkir<KTK>` |p100|
* :mtgexp:`JOU` :doc:`Journey into Nyx<JOU>` |p100|
* :mtgexp:`BNG` :doc:`Born of the Gods<BNG>` |p100|
* :mtgexp:`THS` :doc:`Theros<THS>` |p100|
* :mtgexp:`DGM` :doc:`Dragon's Maze<DGM>` |p100|
* :mtgexp:`GTC` :doc:`Gatecrash<GTC>` |p100|
* :mtgexp:`RTR` :doc:`Return to Ravnica<RTR>` |p100|

Edycje bazowe
-------------

* :mtgexp:`M19` :doc:`Core Set 2019<M19>` |p100|
* :mtgexp:`M15` :doc:`Magic 2015<M15>` |p100|
* :mtgexp:`M14` :doc:`Magic 2014<M14>` |p100|


.. toctree::
   :hidden:

   M19
   DOM
   RIX
   XLN
   MH1
   HOU
   AKH
   AER
   KLD
   EMN
   SOI
   OGW
   BFZ
   ORI
   DTK
   FRF
   KTK
   M15
   JOU
   BNG
   THS
   M14
   DGM
   GTC
   RTR
