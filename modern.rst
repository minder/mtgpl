******
Modern
******

.. include:: progress.rst

Standard
--------

Format Standard jest rotującym formatem, tzn. raz w roku najstarsze dodatki
obecne w tym formacie przestają być "legalne" - zostają wyłączone z formatu,
czyli "rotują". Na ich miejsce przychodzą nowo wydawane dodatki.

Lista dodatków obecnych w aktualnym Standardzie znajduje się
:doc:`na stronie głównej<index>`.

Karty z dodatków, które wyszły ze Standardu, nadal mogą być grane w formatach
:doc:`Pionier<pioneer>`, :doc:`Modern<modern>` i :doc:`Legacy<legacy>`, a format
Standard jest ich podzbiorem.

Dodatki specjalne
-----------------

Poniższy dodatek należy do formatu Modern, choć nigdy nie był częścią Standardu.

* :mtgexp:`MH1` :doc:`Modern Horizons<MH1>` |p100|

Pioneer
-------

Format :doc:`Pionier<pioneer>` jest podzbiorem formatu Modern.

Dodatki poza Pionierem
----------------------

* :mtgexp:`AVR` :doc:`Avacyn Restored<AVR>` |p30|
* :mtgexp:`DKA` :doc:`Dark Ascension<DKA>` |p20|
* :mtgexp:`ISD` :doc:`Innistrad<ISD>` |p25|
* :mtgexp:`NPH` :doc:`New Phyrexia<NPH>` |p15|
* :mtgexp:`MBS` :doc:`Mirrodin Besieged<MBS>` |p15|
* :mtgexp:`SOM` :doc:`Scars of Mirrodin<SOM>` |p15|
* :mtgexp:`ROE` :doc:`Rise of the Eldrazi<ROE>` |p25|
* :mtgexp:`WWK` :doc:`Worldwake<WWK>` |p15|
* :mtgexp:`ZEN` :doc:`Zendikar<ZEN>` |p15|
* :mtgexp:`ARB` :doc:`Alara Reborn<ARB>` |p20|
* :mtgexp:`CON` :doc:`Conflux<CON>` |p15|
* :mtgexp:`ALA` :doc:`Shards of Alara<ALA>` |p15|
* :mtgexp:`EVE` :doc:`Eventide<EVE>` |p10|
* :mtgexp:`SHM` :doc:`Shadowmoor<SHM>` |p10|
* :mtgexp:`MOR` :doc:`Morningtide<MOR>` |p10|
* :mtgexp:`LRW` :doc:`Lorwyn<LRW>` |p10|
* :mtgexp:`FUT` :doc:`Future Sight<FUT>` |p15|
* :mtgexp:`PLC` :doc:`Planar Chaos<PLC>` |p10|
* :mtgexp:`TSP` :doc:`Time Spiral<TSP>` |p10|
* :mtgexp:`CSP` :doc:`Coldsnap<CSP>` |p10|
* :mtgexp:`DIS` :doc:`Dissension<DIS>` |p10|
* :mtgexp:`GPT` :doc:`Guildpact<GPT>` |p20|
* :mtgexp:`RAV` :doc:`Ravnica: City of Guilds<RAV>` |p20|
* :mtgexp:`SOK` :doc:`Saviors of Kamigawa<SOK>` |p05|
* :mtgexp:`BOK` :doc:`Betrayers of Kamigawa<BOK>` |p05|
* :mtgexp:`CHK` :doc:`Champions of Kamigawa<CHK>` |p10|
* :mtgexp:`5DN` :doc:`Fifth Dawn<5DN>` |p10|
* :mtgexp:`DST` :doc:`Darksteel<DST>` |p15|
* :mtgexp:`MRD` :doc:`Mirrodin<MRD>` |p10|

Edycje bazowe
-------------

* :mtgexp:`M13` :doc:`Magic 2013<M13>` |p25|
* :mtgexp:`M12` :doc:`Magic 2012<M12>` |p25|
* :mtgexp:`M11` :doc:`Magic 2011<M11>` |p25|
* :mtgexp:`M10` :doc:`Magic 2010<M10>` |p15|
* :mtgexp:`10E` :doc:`Tenth Edition<10E>` |p15|
* :mtgexp:`9ED` :doc:`Ninth Edition<9ED>` |p15|
* :mtgexp:`8ED` :doc:`Eighth Edition<8ED>` |p15|


.. toctree::
   :hidden:

   pioneer
   M13
   AVR
   DKA
   ISD
   M12
   NPH
   MBS
   SOM
   M11
   ROE
   WWK
   ZEN
   M10
   ARB
   CON
   ALA
   EVE
   SHM
   MOR
   LRW
   10E
   FUT
   PLC
   TSP
   CSP
   DIS
   GPT
   RAV
   9ED
   SOK
   BOK
   CHK
   5DN
   DST
   MRD
   8ED
