.. include:: progress.rst

**************************************
 Tłumaczenie gry Magic: The Gathering
**************************************

Ninieszy serwis ma na celu pomoc osobom, które chcą zacząć grać
w Magic: the Gathering, ale nie znają angielskiego na tyle dobrze,
by samodzielnie tłumaczyć karty i zasady gry.

Po najechaniu myszką na nazwę karty można zobaczyć jej oryginalny wizerunek.

Jeśli uważasz, że projekt ten jest przydatny, możesz `wesprzeć autora na Patronite <https://patronite.pl/mtgpl>`_.

Instrukcje
==========

:doc:`Podstawowy podręcznik zasad<rulebook>` |p100|

.. :doc:`Kompletne Zasady<comprules>` (0%)

Tłumaczenie dodatków
====================

Nadchodzące
------------

.. * :mtgexp:`XZNR` Zendikar Rising (2020-10)
* :mtgexp:`M21` Core Set 2021 (2020-07)
* :mtgexp:`IKO` :doc:`Ikoria: Lair of Behemots<IKO>` (2020-04-17)


Standard
--------

* :mtgexp:`THB` :doc:`Theros: Beyond Death<THB>` |p100|
* :mtgexp:`ELD` :doc:`Throne of Eldraine<ELD>` |p100|
* :mtgexp:`M20` :doc:`Core Set 2020<M20>` |p100|
* :mtgexp:`WAR` :doc:`War of the Spark<WAR>` |p100|
* :mtgexp:`RNA` :doc:`Ravnica Allegiance<RNA>` |p100|
* :mtgexp:`GRN` :doc:`Guilds of Ravnica<GRN>` |p100|

Pioneer
-------

Pionier jest nowym formatem bez rotacji. Należą do niego wszystkie dodatki wydane od :doc:`Return to Ravnica<RTR>`.

* :doc:`Pionier<pioneer>`

Modern
------

Modern jest jednym z formatów pozbawionych rotacji. Należą do niego wszystkie dodatki wydane od :doc:`Mirrodin<MRD>`.

* :doc:`Pełna lista dodatków wchodzących w skład formatu Modern<modern>`

Oprócz tego format otrzymał specjalny dodatek, który nigdy nie był częścią Standardu:

* :mtgexp:`MH1` :doc:`Modern Horizons<MH1>` |p100|

Legacy / Vintage
----------------

Formaty Legacy i Vintage zawierają wszystkie karty wydane od początku istnienia
gry. Na poniższej liście znajdują się te, które są starsze od wymienionych powyżej.

* :doc:`Lista dodatków<legacy>`


Pozostałe produkty
==================

Zestawy gildyjne
----------------

* :mtgexp:`azorius` :mtgexp:`gruul` :mtgexp:`rakdos` :mtgexp:`simic` :mtgexp:`orzhov` :doc:`Ravnica Allegiance Kits<GK2>` |p25|
* :mtgexp:`dimir` :mtgexp:`selesnya` :mtgexp:`izzet` :mtgexp:`boros` :mtgexp:`golgari` :doc:`Guilds of Ravnica Kits<GK1>` |p100|


Seria 'Masters'
---------------

* :mtgexp:`UMA` :doc:`Ultimate Masters<UMA>` 2018 |p25|
* :mtgexp:`A25` :doc:`Masters 25<A25>` 2018 |p25|
* :mtgexp:`IMA` :doc:`Iconic Masters<IMA>` 2017 |p25|
* :mtgexp:`MM3` :doc:`Modern Masters 2017<MM3>` |p25|
* :mtgexp:`EMA` :doc:`Eternal Masters<EMA>` 2016 |p25|
* :mtgexp:`MM2` :doc:`Modern Masters 2015<MM2>` |p25|
* :mtgexp:`MMA` :doc:`Modern Masters<MMA>` 2013 |p25|

Commander i pokrewne
--------------------

:doc:`Zasady formatu Commander (Dowódca)<commander>` |p25|

* :mtgexp:`C19` :doc:`Commander 2019<C19>` |p50|
* :mtgexp:`C18` :doc:`Commander 2018<C18>` |p25|
* :mtgexp:`BBD` :doc:`Battlebond<BBD>` |p25|
* :mtgexp:`C17` :doc:`Commander 2017<C17>` |p25|
* :mtgexp:`C16` :doc:`Commander 2016<C16>` |p25|
* :mtgexp:`C15` :doc:`Commander 2015<C15>` |p25|
* :mtgexp:`C14` :doc:`Commander 2014<C14>` |p25|
* :mtgexp:`C13` :doc:`Commander 2013<C13>` |p25|

Conspiracy
----------

* :mtgexp:`CN2` :doc:`Conspiracy: Take the Crown<CN2>` |p25|
* :mtgexp:`CNS` :doc:`Conspiracy<CNS>` |p25|

Pozostałe produkty
==================

* :mtgexp:`GNT` :doc:`Game Night<GNT>` |p100|
* :mtgexp:`E02` :doc:`Explorers of Ixalan<E02>` |p100|

Poniższe produkty nie zawierają zwykłych kart do Magica, ale umożliwiają specjalne rodzaje rozgrywek.

* :mtgexp:`MTG` :doc:`Bohaterowie<THS_heroes>` |p100|
* :mtgexp:`JOU` :doc:`Pokonaj Bóstwo<JOU_god>` |p100|
* :mtgexp:`BNG` :doc:`Walka z Hordą<BNG_horde>` |p100|
* :mtgexp:`THS` :doc:`Staw czoła Hydrze<THS_hydra>` |p100|

Warto przeczytać
================

* :doc:`Uzasadnienie tłumaczeń<rationale>`
* :doc:`Nowe sposoby gry<game_variants>`
* :doc:`Format Handicap<handicap>`
* :doc:`Polskie lądy i żetony<polish_cards>`


Glosariusz
==========

* :doc:`Słowniczek podstawowych pojęć oraz slangu<glossary>`

Ostatnie zmiany
===============
Proszę zajrzeć do pliku :doc:`Ostatnie zmiany<changelog>`

Ekipa
=====

Niniejsze tłumaczenie powstało dzięki pracy następujących osób:

* Dominik Kozaczko - pomysłodawca, główny tłumacz
* Bartek Pękala - tłumacz
* Krzysztof Buniewicz - tłumacz
* Zofia Wąchocka - tłumacz, filolog
* Roman Cupek - nadzorca merytoryczny

Kwestie prawne
==============

* Niniejszy serwis działa zgodnie z zasadami zawartymi w dokumencie
  `Wizards of the Coast's Fan Content Policy <https://company.wizards.com/fancontentpolicy>`_ (lub przynajmniej mocno się stara).
* Zawartość niniejszego serwisu NIE JEST zatwierdzona lub sponsorowana przez Wizards of the Coast LLC.
* Tłumaczenia zawarte w tym serwisie (CC) copyleft Dominik Kozaczko. Some rights reversed.
* Licencja tłumaczeń: `CC BY-NC-SA 4.0 <https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode.pl>`_ (licencja ta nie dotyczy Wizards of the Coast LLC.)

Legal notice
------------

* MTGpoPolsku.pl is unofficial Fan Content permitted under the `Fan Content Policy <https://company.wizards.com/fancontentpolicy>`_. Not approved/endorsed by Wizards. Portions of the materials used are property of Wizards of the Coast. ©Wizards of the Coast LLC.
* Translation work `(CC) copyleft <https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode>`_ Dominik Kozaczko. Some rights reversed.

.. toctree::
   :hidden:

   rulebook
   THB
   ELD
   M20
   WAR
   RNA
   GRN
   M19
   DOM
   RIX
   XLN

   MH1
   modern
   legacy

   E02
   BBD
   C18
   C17
   C16
   C15
   C14
   C13

   CNS
   CN2

   EMA
   GK2
   GK1
   GNT
   IMA
   MM2
   MM3
   MMA
   UMA
   A25

   JOU_god
   THS_heroes
   THS_hydra
   BNG_horde

   commander
   commander_banned

   game_variants
   handicap
   rationale
   polish_cards
   symbols
   glossary
   changelog
