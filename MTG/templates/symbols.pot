# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2013-2020, Dominik Kozaczko
# This file is distributed under the same license as the MTG-pl package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: MTG-pl 2020.3\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-04-05 00:30+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
