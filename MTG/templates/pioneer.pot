# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2013-2020, Dominik Kozaczko
# This file is distributed under the same license as the MTG-pl package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: MTG-pl 2020.3\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-04-05 00:30+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../pioneer.rst:3
msgid "Pionier"
msgstr ""

#: ../../pioneer.rst:8
msgid "Standard"
msgstr ""

#: ../../pioneer.rst:10
msgid "Format Standard jest rotującym formatem, tzn. raz w roku najstarsze dodatki obecne w tym formacie przestają być \"legalne\" - zostają wyłączone z formatu, czyli \"rotują\". Na ich miejsce przychodzą nowo wydawane dodatki."
msgstr ""

#: ../../pioneer.rst:14
msgid "Lista dodatków obecnych w aktualnym Standardzie znajduje się :doc:`na stronie głównej<index>`."
msgstr ""

#: ../../pioneer.rst:17
msgid "Karty z dodatków, które wyszły ze Standardu, nadal mogą być grane w formatach :doc:`Pionier<pioneer>`, :doc:`Modern<modern>` i :doc:`Legacy<legacy>`, a format Standard jest ich podzbiorem."
msgstr ""

#: ../../pioneer.rst:22
msgid "Dodatki poza Standardem"
msgstr ""

#: ../../pioneer.rst:24
msgid ":mtgexp:`DOM` :doc:`Dominaria<DOM>` |p100|"
msgstr ""

#: ../../pioneer.rst:25
msgid ":mtgexp:`RIX` :doc:`Rivals of Ixalan<RIX>` |p100|"
msgstr ""

#: ../../pioneer.rst:26
msgid ":mtgexp:`XLN` :doc:`Ixalan<XLN>` |p100|"
msgstr ""

#: ../../pioneer.rst:27
msgid ":mtgexp:`HOU` :doc:`Hour of Devastation<HOU>` |p100|"
msgstr ""

#: ../../pioneer.rst:28
msgid ":mtgexp:`AKH` :doc:`Amonkhet<AKH>` |p100|"
msgstr ""

#: ../../pioneer.rst:29
msgid ":mtgexp:`AER` :doc:`Aether Revolt<AER>` |p100|"
msgstr ""

#: ../../pioneer.rst:30
msgid ":mtgexp:`KLD` :doc:`Kaladesh<KLD>` |p100|"
msgstr ""

#: ../../pioneer.rst:31
msgid ":mtgexp:`EMN` :doc:`Eldritch Moon<EMN>` |p100|"
msgstr ""

#: ../../pioneer.rst:32
msgid ":mtgexp:`SOI` :doc:`Shadows over Innistrad<SOI>` |p100|"
msgstr ""

#: ../../pioneer.rst:33
msgid ":mtgexp:`OGW` :doc:`Oath of the Gatewatch<OGW>` |p100|"
msgstr ""

#: ../../pioneer.rst:34
msgid ":mtgexp:`BFZ` :doc:`Battle for Zendikar<BFZ>` |p100|"
msgstr ""

#: ../../pioneer.rst:35
msgid ":mtgexp:`ORI` :doc:`Origins<ORI>` |p100|"
msgstr ""

#: ../../pioneer.rst:36
msgid ":mtgexp:`DTK` :doc:`Dragons of Tarkir<DTK>` |p100|"
msgstr ""

#: ../../pioneer.rst:37
msgid ":mtgexp:`FRF` :doc:`Fate Reforged<FRF>` |p100|"
msgstr ""

#: ../../pioneer.rst:38
msgid ":mtgexp:`KTK` :doc:`Khans of Tarkir<KTK>` |p100|"
msgstr ""

#: ../../pioneer.rst:39
msgid ":mtgexp:`JOU` :doc:`Journey into Nyx<JOU>` |p100|"
msgstr ""

#: ../../pioneer.rst:40
msgid ":mtgexp:`BNG` :doc:`Born of the Gods<BNG>` |p100|"
msgstr ""

#: ../../pioneer.rst:41
msgid ":mtgexp:`THS` :doc:`Theros<THS>` |p100|"
msgstr ""

#: ../../pioneer.rst:42
msgid ":mtgexp:`DGM` :doc:`Dragon's Maze<DGM>` |p100|"
msgstr ""

#: ../../pioneer.rst:43
msgid ":mtgexp:`GTC` :doc:`Gatecrash<GTC>` |p100|"
msgstr ""

#: ../../pioneer.rst:44
msgid ":mtgexp:`RTR` :doc:`Return to Ravnica<RTR>` |p100|"
msgstr ""

#: ../../pioneer.rst:47
msgid "Edycje bazowe"
msgstr ""

#: ../../pioneer.rst:49
msgid ":mtgexp:`M19` :doc:`Core Set 2019<M19>` |p100|"
msgstr ""

#: ../../pioneer.rst:50
msgid ":mtgexp:`M15` :doc:`Magic 2015<M15>` |p100|"
msgstr ""

#: ../../pioneer.rst:51
msgid ":mtgexp:`M14` :doc:`Magic 2014<M14>` |p100|"
msgstr ""
