****************
Legacy / Vintage
****************

W tej grupie znajdują się najstarsze dodatki posiadające pierwotny wygląd ramek.
Formaty Vintage oraz Legacy zawierają w sobie także dodatki
z :doc:`Moderna<modern>`, :doc:`Pioniera<pioneer>` i Standardu.

UWAGA: Teksty kart poniższych dodatków tłumaczone są na podstawie ich treści z bazy wiedzy o kartach "Oracle",
która jest głównym wyznacznikiem reguł kart. Mogą przez to znacząco odbiegać od oryginalnie wydrukowanych treści.

.. include:: progress.rst

Dodatki
-------

* :mtgexp:`SCG` :doc:`Scourge<SCG>` |p25|
* :mtgexp:`LGN` :doc:`Legions<LGN>` |p25|
* :mtgexp:`ONS` :doc:`Onslaught<ONS>` |p25|
* :mtgexp:`JUD` :doc:`Judgement<JUD>` |p25|
* :mtgexp:`TOR` :doc:`Torment<TOR>` |p25|
* :mtgexp:`ODY` :doc:`Odyssey<ODY>` |p25|
* :mtgexp:`APC` :doc:`Apocalypse<APC>` |p25|
* :mtgexp:`PLS` :doc:`Planeshift<PLS>` |p25|
* :mtgexp:`INV` :doc:`Invasion<INV>` |p25|
* :mtgexp:`PCY` :doc:`Prophecy<PCY>` |p25|
* :mtgexp:`NEM` :doc:`Nemesis<NEM>` |p25|
* :mtgexp:`MMQ` :doc:`Mercadian Masques<MMQ>` |p25|
* :mtgexp:`UDS` :doc:`Urza's Destiny<UDS>` |p25|
* :mtgexp:`ULG` :doc:`Urza's Legacy<ULG>` |p25|
* :mtgexp:`USG` :doc:`Urza's Saga<USG>` |p25|
* :mtgexp:`EXO` :doc:`Exodus<EXO>` |p25|
* :mtgexp:`STH` :doc:`Stronghold<STH>` |p25|
* :mtgexp:`TMP` :doc:`Tempest<TMP>` |p25|
* :mtgexp:`WTH` :doc:`Weatherlight<WTH>` |p25|
* :mtgexp:`VIS` :doc:`Visions<VIS>` |p25|
* :mtgexp:`MIR` :doc:`Mirage<MIR>` |p25|
* :mtgexp:`ALL` :doc:`Alliances<ALL>` |p25|
* :mtgexp:`HML` :doc:`Homelands<HML>` |p25|
* :mtgexp:`ICE` :doc:`Ice Age<ICE>` |p25|
* :mtgexp:`FEM` :doc:`Fallen Empires<FEM>` |p25|
* :mtgexp:`DRK` :doc:`The Dark<DRK>` |p25|
* :mtgexp:`LEG` :doc:`Legends<LEG>` |p25|
* :mtgexp:`ATQ` :doc:`Antiquities<ATQ>` |p25|
* :mtgexp:`ARN` :doc:`Arabian Nights<ARN>` |p25|

Edycje bazowe
-------------

* :mtgexp:`7ED` :doc:`Seventh Edition<7ED>` |p25|
* :mtgexp:`6ED` :doc:`Sixth Edition<6ED>` |p25|
* :mtgexp:`5ED` :doc:`Fifth Edition<5ED>` |p25|
* :mtgexp:`4ED` :doc:`Fourth Edition<4ED>` |p25|
* :mtgexp:`3ED` :doc:`Revised Edition<3ED>` |p25|
* :mtgexp:`2ED` :doc:`Unlimited Edition<2ED>` |p25|
* :mtgexp:`LEB` :doc:`Beta (Limited Edition)<LEB>` |p25|
* :mtgexp:`LEA` :doc:`Alpha (Limited Edition)<LEA>` |p25|

.. toctree::
   :hidden:

   SCG
   LGN
   ONS
   JUD
   TOR
   ODY
   APC
   PLS
   INV
   PCY
   NEM
   MMQ
   UDS
   ULG
   USG
   EXO
   STH
   TMP
   WTH
   VIS
   MIR
   ALL
   HML
   ICE
   FEM
   DRK
   LEG
   ATQ
   ARN

   7ED
   6ED
   5ED
   4ED
   3ED
   2ED
   LEB
   LEA
